#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define NUMBER 10
#define NOTHREADS 5
//Data structure for the whole thing
typedef struct Data {
    int *v1;
    int *v2;
    int result;
    int start;
    int numberPerThread;
} Data;

//ufnction will be executed by threads
void* multi (void* argument) {
    Data* args = (Data*) argument;

    for (int j = 0; j < args->numberPerThread; j++) {
        args->result += args->v2[args->start + j] * args->v1[args->start + j];
    }
}

int main() {
    //random seed
    srand(time(NULL));

    //allocate space
    int *v1 = (int*) malloc(NUMBER*sizeof(int));
    int *v2 = (int*) malloc(NUMBER*sizeof(int));

    for (int i = 0; i < NUMBER; i++) {
        v1[i] = rand() % 25;
        v2[i] = rand() % 25;
    }
    //create threads space
    pthread_t threadHandler[NOTHREADS];
    
    int position = 0;
    int numberPerThread = NUMBER/NOTHREADS;

    //create array of pointers to thread arguments "Data" structure
    Data* threadArg[NOTHREADS];

    //create threads
    for (int i = 0; i < NOTHREADS; i++) {
        //Create argument obj
        Data *arg = (Data*) malloc(sizeof(Data));
        //Pass attribute to arg obj
        arg->v1 = v1;
        arg->v2 = v2;
        arg->start = position;
        arg->numberPerThread = numberPerThread;
        arg->result = 0;
        threadArg[i] = arg;
        position += numberPerThread;
        //create thread
        pthread_create(&threadHandler[i], NULL, multi, (void*) threadArg[i]);
    }
    
    //join threads
    for (int i = 0; i < NOTHREADS; i++) {
        pthread_join(threadHandler[i], NULL);
    }

    //calculate the final result.
    int rs = 0;
    for (int i = 0; i < NOTHREADS; i++) {
        printf("At thread i: %d  sum: %d\n", i, threadArg[i]->result);
        rs += threadArg[i]->result;
    }
    printf("%d\n", rs);

    for (int i = 0; i < NUMBER; i++) {
        printf("%d ", v1[i]);
    }

    printf("\n");
    for (int i = 0; i < NUMBER; i++) {
        printf("%d ", v2[i]);
    }
    return 0;
}